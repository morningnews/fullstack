# Morning News - Frontend

## Installation

```bash
yarn install
```

___

## Start in dev environment

1- Create **.env** file containing the following environement variables:
- BACKEND_URL : URL to local backend without trailing '/' (ex: "http://localhost:3000")

2- Start development server with **yarn**

```bash
yarn dev
```

___

## Launch tests

### Unit tests with Jest

```bash
yarn test:jest
```

### E2E tests with Cypress

```bash
yarn test:cypress
```

___

## Create production build

1- Create **.env** file containing the environement following variables:
- BACKEND_URL : URL to backend in production without trailing '/' (ex: "http://backend.morningnews.com")

2- Build and generate with **yarn**

```bash
yarn build
```

Static files will be generated in `./out` folder.

___

## Linter

```bash
yarn lint
```
