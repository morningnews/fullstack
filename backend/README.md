# Morning News - Backend

## Installation

```shell
yarn install
```

___

## Start in dev environment

1- Create **.env** file containing the following environment variables:
- CONNECTION_STRING : URI to MongoDB database (ex: "mongodb+srv://...")
- NEWS_API_KEY : API key to newsapi.org (ex: "1c0a5...")

2- Start development server with **yarn**

```shell
yarn start
```

___

## Launch tests

### Unit tests with Jest

```bash
yarn test
```

___

## Linter

```bash
yarn lint
```
